#
# Cookbook Name:: phpmyadmin
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

cookbook_file "/home/vagrant/phpMyAdmin-4.1.6-all-languages.tar.gz" do
  mode 0644
end

bash "install phpMyAdmin" do
  user     "root"
  cwd      "/home/vagrant"
  code   <<-EOH
    tar xzf phpMyAdmin-4.1.6-all-languages.tar.gz
    mv phpMyAdmin-4.1.6-all-languages /var/lib/phpMyAdmin/
  EOH
  not_if { ::File.exists?("/var/lib/phpMyAdmin/index.php") }
end

link "/var/www/html/phpMyAdmin" do
  to "/var/lib/phpMyAdmin"
end

template "config.inc.php" do
        path "/var/lib/phpMyAdmin/config.inc.php"
        source "config.inc.php.erb"
        owner "root"
        group "root"
        mode 0644
end
