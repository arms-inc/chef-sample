#
# Cookbook Name:: php
# Recipe:: xdebug
#
# Copyright 2014, Arms.inc
#
# All rights reserved - Do Not Redistribute
#

bash "install xdebug" do
  user     "root"
  code   <<-EOH
    pecl install xdebug
  EOH
  not_if "pecl list xdebug"
  notifies :restart , "service[httpd]"
end

template "xdebug.ini" do
        path "/etc/php.d/xdebug.ini"
        source "xdebug.ini.erb"
        owner "root"
        group "root"
        mode 0644
        notifies :restart , "service[httpd]"
end

