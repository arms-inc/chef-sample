#
# Cookbook Name:: php
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

if node.key?('php') && node['php'].key?('version') && node['php']['version'] == '5.4' then
	include_recipe "php::php54"
else
	include_recipe "php::php53"
end
