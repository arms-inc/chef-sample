#
# Cookbook Name:: php
# Recipe:: php54
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
# 

bash 'yum_update_postfix' do
  code <<-"EOH"
      yum -y update postfix
  EOH
  action :run
  not_if "yum list installed | grep php"
end

%w{php php-common php-mbstring php-xml php-devel php-process php-cli php-gd php-mysql}.each do |p|
    package p do
        options "--enablerepo=remi"
        #version "5.4.31-1.el6.remi"
        action :install
        notifies :restart , "service[httpd]"
    end
end

package "php-pear" do
    options "--enablerepo=remi"
    action :install
    notifies :restart , "service[httpd]"
end


template "php.ini" do
	path "/etc/php.ini"
	source "php54.ini.erb"
	owner "root"
	group "root"
	mode 0644
	notifies :restart , "service[httpd]"
end
