#
# Cookbook Name:: php
# Recipe:: php53
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

%w{php php-common php-mbstring php-xml php-devel php-process php-cli php-gd php-mysql php-pear}.each do |p|
        package p do
            action :install
    end
end

template "php.ini" do
	path "/etc/php.ini"
	source "php53.ini.erb"
	owner "root"
	group "root"
	mode 0644
	notifies :restart , "service[httpd]"
end
