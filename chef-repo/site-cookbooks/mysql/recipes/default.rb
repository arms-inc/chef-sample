#
# Cookbook Name:: mysql
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

%w{mysql-server mysql-devel}.each do |p|
        package p do
        action :install
        options "--enablerepo=remi"
    end
end

service "mysqld" do
  supports :status => true, :restart => true, :reload => true
  action [ :enable , :start ]
end

if node.key?('mysql') && node['mysql'].key?('db_name') && node['mysql']['db_name'] != '' then
    bash 'mysql_create_db' do
      code <<-"EOH"
          /usr/bin/mysql  -u root -e "CREATE DATABASE #{node['mysql']['db_name']} character set utf8 collate utf8_general_ci;"
      EOH
      action :run
      only_if "/usr/bin/mysql -u root -e 'show databases;'"
    end
end

bash 'mysql_secure_install emulate' do
  code <<-"EOH"
      /usr/bin/mysql  -u root -e "DELETE FROM mysql.user WHERE User='';"
      /usr/bin/mysql  -u root -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1');"
      /usr/bin/mysql  -u root -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"
      /usr/bin/mysql  -u root -e "DROP DATABASE test;"
      /usr/bin/mysql  -u root -e "SET PASSWORD FOR 'root'@'127.0.0.1' = PASSWORD('vagrant');" -D mysql
      /usr/bin/mysql  -u root -e "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('vagrant');" -D mysql
      /usr/bin/mysql  -u root -e "FLUSH PRIVILEGES;" -pvagrant
  EOH
  action :run
  only_if "/usr/bin/mysql -u root -e 'show databases;'"
  notifies :restart , 'service[mysqld]'
end

template "my.cnf" do
	path "/etc/my.cnf"
	source "my.cnf55.erb"
	owner "root"
	group "root"
	mode 0644
	notifies :restart , 'service[mysqld]'
end
