#
# Cookbook Name:: httpd
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#if node.key?('httpd_server') && node['httpd_server'].key?('name') && node['httpd_server']['name'] == 'apache' then
#	include_recipe "httpd::apache"
#else
#	include_recipe "nginx::default"
#end

%w{httpd httpd-devel}.each do |p|
        package p do
            action :install
    end
end

service "httpd" do
  supports :status => true, :restart => true, :reload => true
  action [ :enable , :start ]
end

template "httpd.conf" do
        path "/etc/httpd/conf/httpd.conf"
        source "httpd.conf.erb"
        owner "root"
        group "root"
        mode 0644
        notifies :restart , "service[httpd]"
end
